<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191210090621 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE minerais (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, content VARCHAR(255) NOT NULL, date DATETIME NOT NULL, danger NUMERIC(2, 2) NOT NULL, INDEX IDX_A6DECC4AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE minerais_zones (minerais_id INT NOT NULL, zones_id INT NOT NULL, INDEX IDX_96AA9E5C1D68AA11 (minerais_id), INDEX IDX_96AA9E5CA6EAEB7A (zones_id), PRIMARY KEY(minerais_id, zones_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE minerais ADD CONSTRAINT FK_A6DECC4AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE minerais_zones ADD CONSTRAINT FK_96AA9E5C1D68AA11 FOREIGN KEY (minerais_id) REFERENCES minerais (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE minerais_zones ADD CONSTRAINT FK_96AA9E5CA6EAEB7A FOREIGN KEY (zones_id) REFERENCES zones (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE minerais_zones DROP FOREIGN KEY FK_96AA9E5C1D68AA11');
        $this->addSql('DROP TABLE minerais');
        $this->addSql('DROP TABLE minerais_zones');
    }
}
