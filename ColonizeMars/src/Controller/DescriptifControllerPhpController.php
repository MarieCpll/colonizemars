<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Minerais;

class DescriptifControllerPhpController extends AbstractController
{
    /**
     * @Route("/descriptif", name="descriptif_controller_php")
     */
    public function index()
    {
        return $this->render('descriptif_controller_php/index.html.twig', [
            'controller_name' => 'DescriptifControllerPhpController',
        ]);
    }
	/**
     * @Route("/klingon", name="descriptif_klingon")
     */
    public function klingon(){

		$repo = $this->getDoctrine()->getRepository(Minerais::class);

        $minerais = $repo->find(2);
//		var_dump($minerais);

		return $this->render('descriptif_controller_php/klingon.html.twig', [
			'title' => "klingon",
			'controller_name' => 'DescriptifControllerPhpController',
            'minerais' => $minerais,
		]);

	}
	/**
     * @Route("/chomdu", name="chomdu.html.twig")
     */
    public function chomdu(){

		$repo = $this->getDoctrine()->getRepository(Minerais::class);

        $minerais = $repo->find(3);
//		var_dump($minerais);

		return $this->render('descriptif_controller_php/chomdu.html.twig', [
			'title' => "chomdu",
			'controller_name' => 'DescriptifControllerPhpController',
            'minerais' => $minerais,
		]);

	}

	/**
     * @Route("/perl", name="perl.html.twig")
     */
    public function perl(){

		$repo = $this->getDoctrine()->getRepository(Minerais::class);

        $minerais = $repo->find(4);


		return $this->render('descriptif_controller_php/perl.html.twig', [
			'title' => "perl",
			'controller_name' => 'DescriptifControllerPhpController',
            'minerais' => $minerais,
		]);

	}
}
