<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Minerais;
use App\Entity\Zones;
use App\Entity\User;

class HomeControllerPhpController extends AbstractController
{
    /**
     * @Route("/", name="home_show")
     */
    public function index()
    {
        return $this->render('home_controller_php/index.html.twig', [
            'controller_name' => 'HomeControllerPhpController',

        ]);

    }
     /**
     * @Route("/home", name="home_show")
     */
    public function home(){

		$repo = $this->getDoctrine()->getRepository(Zones::class);

        $zone = $repo->findAll();


		return $this->render('home_controller_php/home.html.twig', [
			'title' => "Bienvenue",
			'controller_name' => 'HomeControllerPhpController',
            'zones' => $zone,
		]);

	}

}
