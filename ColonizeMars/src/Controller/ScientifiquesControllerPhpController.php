<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use App\Entity\Minerais;
use App\Entity\Zones;
use App\Entity\User;

class ScientifiquesControllerPhpController extends AbstractController
{
    /**
     * @Route("/scientifiques", name="scientifiques_controller_php")
     */
    public function index()
    {
    	$repo = $this->getDoctrine()->getRepository(Zones::class);

        $zone = $repo->findAll();

        return $this->render('scientifiques_controller_php/index.html.twig', [
            'controller_name' => 'ScientifiquesControllerPhpController',
            'title' => "Bienvenue",
            'zones' => $zone,
        ]);
    }
}
