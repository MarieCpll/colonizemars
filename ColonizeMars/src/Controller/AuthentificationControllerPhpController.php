<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class AuthentificationControllerPhpController extends AbstractController
{
    /**
     * @Route("/authentification", name="authentification_controller_php")
     */
    public function index()
    {
        return $this->render('authentification_controller_php/index.html.twig', [
            'controller_name' => 'AuthentificationControllerPhpController',
        ]);
    }
}
