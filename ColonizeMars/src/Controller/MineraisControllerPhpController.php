<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MineraisControllerPhpController extends AbstractController
{
    /**
     * @Route("/minerais", name="minerais_controller_php")
     */
    public function index()
    {
        return $this->render('minerais_controller_php/index.html.twig', [
            'controller_name' => 'MineraisControllerPhpController',
        ]);
    }
}
