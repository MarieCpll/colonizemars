<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class IdentificationControllerPhpController extends AbstractController
{
    /**
     * @Route("/", name="identification_controller_php")
     */
    public function index()
    {
        return $this->render('identification_controller_php/index.html.twig', [
            'controller_name' => 'IdentificationControllerPhpController',
        ]);
    }
}
