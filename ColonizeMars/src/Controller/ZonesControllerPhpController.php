<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
//use Doctrine\Common\Persistence\ObjectManager;
//use Doctrine\Orm\default_entity_manager;
//use App\Entity\Zones;
//use Doctrine\ORM\EntityManager;
//use App\Controller\ObjectManager;
 
 

class ZonesControllerPhpController extends AbstractController
{

    // private $entityManager;
    // public function __construct(EntityManager $entityManager)
    // {
    //     $this->entityManager = $entityManager;


    // $this->entityManager->persist($entityManager);
    // }



    /**
     * @Route("/index", name="zones_controller_php")
     */
    public function index()
    {
        return $this->render('zones_controller_php/index.html.twig', [
            'controller_name' => 'ZonesControllerPhpController',
        ]);
    }

     /**
     * @Route("/zones", name="formulaire_zones")
     */
    public function formulaire(Request $requette)
    {
// a mettre dans les paramettres function : , ObjectManager $manager
        return $this->render('zones_controller_php/formulaire.html.twig');

        	if ($requette->request->count() > 0) {
        	$zones = new Zones();
        	$zones->setName($requette->request->get('name'))
        		->setCoordonneesGPS($requette->request->get('coordonneesGPS'))
        		->setDanger($requette->request->get('danger'))
        		->setDate(new \DateTime());

       
        	$manager = $this->getDoctrine()->getManager();
			$manager->persist($zones);
			$manager->flush();
        }
    }

}
