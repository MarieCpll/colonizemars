<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentaireRepository")
 */
class Commentaire
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="commentaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Zones", inversedBy="commentaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Zone;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Minerais", inversedBy="commentaires")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Minerai;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->Content;
    }

    public function setContent(string $Content): self
    {
        $this->Content = $Content;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getZone(): ?Zones
    {
        return $this->Zone;
    }

    public function setZone(?Zones $Zone): self
    {
        $this->Zone = $Zone;

        return $this;
    }

    public function getMinerai(): ?Minerais
    {
        return $this->Minerai;
    }

    public function setMinerai(?Minerais $Minerai): self
    {
        $this->Minerai = $Minerai;

        return $this;
    }
}
