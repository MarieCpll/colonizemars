<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $role;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Zones", mappedBy="user")
     */
    private $zones;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Minerais", mappedBy="User")
     */
    private $minerais;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commentaire", mappedBy="User")
     */
    private $commentaires;

    public function __construct()
    {
        $this->zones = new ArrayCollection();
        $this->minerais = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(?string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|Zones[]
     */
    public function getZones(): Collection
    {
        return $this->zones;
    }

    public function addZone(Zones $zone): self
    {
        if (!$this->zones->contains($zone)) {
            $this->zones[] = $zone;
            $zone->setUser($this);
        }

        return $this;
    }

    public function removeZone(Zones $zone): self
    {
        if ($this->zones->contains($zone)) {
            $this->zones->removeElement($zone);
            // set the owning side to null (unless already changed)
            if ($zone->getUser() === $this) {
                $zone->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Minerais[]
     */
    public function getMinerais(): Collection
    {
        return $this->minerais;
    }

    public function addMinerai(Minerais $minerai): self
    {
        if (!$this->minerais->contains($minerai)) {
            $this->minerais[] = $minerai;
            $minerai->setUser($this);
        }

        return $this;
    }

    public function removeMinerai(Minerais $minerai): self
    {
        if ($this->minerais->contains($minerai)) {
            $this->minerais->removeElement($minerai);
            // set the owning side to null (unless already changed)
            if ($minerai->getUser() === $this) {
                $minerai->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setUser($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
            // set the owning side to null (unless already changed)
            if ($commentaire->getUser() === $this) {
                $commentaire->setUser(null);
            }
        }

        return $this;
    }
}
