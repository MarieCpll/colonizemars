<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ZonesRepository")
 */
class Zones
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $coordonneesGPS;

    /**
     * @ORM\Column(type="decimal", precision=2, scale=2, nullable=true)
     */
    private $danger;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="zones")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Minerais", mappedBy="Zones")
     */
    private $minerais;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commentaire", mappedBy="Zone")
     */
    private $commentaires;

    public function __construct()
    {
        $this->minerais = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCoordonneesGPS(): ?string
    {
        return $this->coordonneesGPS;
    }

    public function setCoordonneesGPS(string $coordonneesGPS): self
    {
        $this->coordonneesGPS = $coordonneesGPS;

        return $this;
    }

    public function getDanger(): ?string
    {
        return $this->danger;
    }

    public function setDanger(?string $danger): self
    {
        $this->danger = $danger;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUser(): ?user
    {
        return $this->user;
    }

    public function setUser(?user $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Minerais[]
     */
    public function getMinerais(): Collection
    {
        return $this->minerais;
    }

    public function addMinerai(Minerais $minerai): self
    {
        if (!$this->minerais->contains($minerai)) {
            $this->minerais[] = $minerai;
            $minerai->addZone($this);
        }

        return $this;
    }

    public function removeMinerai(Minerais $minerai): self
    {
        if ($this->minerais->contains($minerai)) {
            $this->minerais->removeElement($minerai);
            $minerai->removeZone($this);
        }

        return $this;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setZone($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
            // set the owning side to null (unless already changed)
            if ($commentaire->getZone() === $this) {
                $commentaire->setZone(null);
            }
        }

        return $this;
    }
}
