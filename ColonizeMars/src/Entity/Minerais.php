<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MineraisRepository")
 */
class Minerais
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Title;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Content;

    /**
     * @ORM\Column(type="datetime")
     */
    private $Date;

    /**
     * @ORM\Column(type="decimal", precision=2, scale=2)
     */
    private $Danger;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Zones", inversedBy="minerais")
     */
    private $Zones;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="minerais")
     */
    private $User;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commentaire", mappedBy="Minerai")
     */
    private $commentaires;

    public function __construct()
    {
        $this->Zones = new ArrayCollection();
        $this->commentaires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->Content;
    }

    public function setContent(string $Content): self
    {
        $this->Content = $Content;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->Date;
    }

    public function setDate(\DateTimeInterface $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getDanger(): ?string
    {
        return $this->Danger;
    }

    public function setDanger(string $Danger): self
    {
        $this->Danger = $Danger;

        return $this;
    }

    /**
     * @return Collection|Zones[]
     */
    public function getZones(): Collection
    {
        return $this->Zones;
    }

    public function addZone(Zones $zone): self
    {
        if (!$this->Zones->contains($zone)) {
            $this->Zones[] = $zone;
        }

        return $this;
    }

    public function removeZone(Zones $zone): self
    {
        if ($this->Zones->contains($zone)) {
            $this->Zones->removeElement($zone);
        }

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    /**
     * @return Collection|Commentaire[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setMinerai($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
            // set the owning side to null (unless already changed)
            if ($commentaire->getMinerai() === $this) {
                $commentaire->setMinerai(null);
            }
        }

        return $this;
    }
}
